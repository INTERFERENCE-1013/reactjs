import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import Inicio from './components/JSX/Vistas/Inicio';
import Crud from './components/JSX/Vistas/Crud';
import FuncionesBasicas from './components/JSX/Vistas/FuncionesBasicas';
import Api from './components/JSX/Vistas/Api';
import InformacionUsuario from './components/JSX/Vistas/InformacionUsuario';
import { FaReact } from 'react-icons/fa';

function App() {

  return (
    <Router>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
          <Link className="navbar-brand" to="/"><FaReact size="50" color="teal" className="mr-2"/>ReactJS</Link>
          <button class="navbar-toggler" type="button btn-dark" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="navbar-collapse collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <Link className="nav-link" to="/funciones-basicas">Funciones básicas</Link>
              </li>
              <li class="nav-item">
                <Link className="nav-link" to="/crud">CRUD usando PROPS</Link>
              </li>
              <li class="nav-item">
                <Link className="nav-link" to="/api">Prueba con API</Link>
              </li>
            </ul>
          </div>
        </nav>

        <Switch>
          <Route path="/informacion-usuario/:id">
            <InformacionUsuario/>
          </Route>
          <Route path="/api">
            <Api/>
          </Route>
          <Route path="/funciones-basicas">
            <FuncionesBasicas/>
          </Route>
          <Route path="/crud">
            <Crud/>
          </Route>
          <Route path="/" exact>
            <Inicio/>
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
