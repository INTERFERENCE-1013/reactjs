import React, {useState, Fragment} from 'react';

const Lista = () => {

    const [arrayNumero, setArrayNumero] = useState([1,2,3])

    const [numero, setNumero] = useState(4)

    const agregarElemento = () => {
        setNumero(numero + 1)
        setArrayNumero([...arrayNumero, numero])
    }
    const style = {
        height: "160px",
        overflow: "scroll"
    }

    return (
        <Fragment>
            <div className="card text-white text-center bg-info mb-3">
                <div className="card-header">Agrega elementos a una lista</div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-3 my-auto">
                            <button className="btn-dark" onClick={agregarElemento}>Agregar</button>
                        </div>
                        <div className="col-7 mx-auto">
                            <div style={style}>
                                <ul className="list-group" align="center">
                                    {
                                        arrayNumero.map((item, index) =>
                                        <li className="list-group-item list-group-item-dark" key={index}>Item {item}</li>
                                        )
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Lista;