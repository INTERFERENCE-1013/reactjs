import React, {Fragment, useState} from 'react';

const Jsx = () => {

    const [anio, setAnio] = useState(0)
    const [edad, setEdad] = useState(-1)

    const calcularEdad = () =>{
        if (anio >= 1900 && anio <= 2050) {
            setEdad(2050 - anio)
        }else{
            setEdad(-1)
        }
    }

    return (
        <Fragment>
            <div className="card h-80 text-white text-center bg-info">
                <div className="card-header">Calcula una edad para el año 2050</div>
                <div className="card-body">
                    <input
                    // Asignar valor en el momento
                    onChange={event => setAnio(event.target.value)}
                    placeholder='Año de Nacimiento (Desde 1900)'
                    className="form-control mt-2"
                    />
                    <button className="btn-dark mt-4" onClick={calcularEdad}>Calcular Edad</button>
                    <p if className="card-text">{
                       edad > 1 ? <p>Edad será de {edad} años</p> :
                       edad === 1 ? <p>Edad será de {edad} año</p> :
                       edad === 0 ? <p>Tiene menos de un año</p> :
                       edad === -1 ? <p>*Mensaje de respuesta*</p> :
                       edad === '' ? <p>Introduce un año</p> : <p>Rango invalido</p>
                    }</p>
                </div>
            </div>
        </Fragment>
    );
}

export default Jsx;