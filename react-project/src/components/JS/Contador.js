import React, {useState, Fragment} from 'react';

const Contador = () => {

    const [numero, setNumero] = useState(0);

    const aumentar1 = () => {
        setNumero(numero + 1)
    }
    const aumentar5 = () => {
        setNumero(numero + 5)
    }
    const restar1 = () => {
        setNumero(numero - 1)
    }
    const restar5 = () => {
        setNumero(numero - 5)
    }

    return (
        <Fragment>
            <div className="card text-white text-center bg-info">
                <div className="card-header">Click para modificar el contador</div>
                <div className="card-body">
                    <div className="row mt-3">
                        <div className="col-6 mt-2">
                            <button className="btn-dark btn-block" onClick={aumentar1}>+ 1</button>
                        </div>
                        <div className="col-6 mt-2">
                            <button className="btn-dark btn-block" onClick={aumentar5}>+ 5</button>
                        </div>
                        <div className="col-6 mt-2">
                            <button className="btn-dark btn-block" onClick={restar1}>- 1</button>
                        </div>
                        <div className="col-6 mt-2">
                            <button className="btn-dark btn-block" onClick={restar5}>- 5</button>
                        </div>
                    </div>
                    <p if className="card-text mt-2">Contador: {numero}</p>
                </div>
            </div>
        </Fragment>
    );
}

export default Contador;