import React from 'react'
import { FaReact } from 'react-icons/fa';

const Inicio = () => {
    return (
        <div>
            <div className="jumbotron">
            {/* <div>
                <div className="row justify-center">
                    <div className="card text-white bg-dark mt-5">
                        <div className="card-body">
                            <div className="row justify-center">
                                <FaReact size="200" color="teal"/>
                            </div>
                            <h5 className="card-title text-white text-center">Bienvenidos al projecto de prueba</h5>
                            <p className="card-text">Contiene módulos para revisar sus funcionalidades</p>
                        </div>
                    </div>
                </div>
            </div> */}
                <h1 className="display-4 text-center">Hola, mundo! <FaReact size="100" color="teal"/></h1>
                <p className="lead text-center">Projecto realizado en ReactJS</p>
                <p className="lead text-center">Contiene módulos con diversas funcionalidades</p>
            </div>
        </div>
    );
}

export default Inicio