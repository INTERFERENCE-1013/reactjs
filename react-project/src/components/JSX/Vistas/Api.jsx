import React, {useEffect, useState} from 'react'
import { FcVoicePresentation, FcBusinessContact, FcDepartment } from 'react-icons/fc';
import {Link} from 'react-router-dom'

export default function Api() {

    useEffect(() => {
        obtenerDatos()
    }, [])

    const [usuarios, setUsuarios] = useState([])

    const obtenerDatos = async () => {
        const data = await fetch("https://jsonplaceholder.typicode.com/users")
        const users = await data.json()
        setUsuarios(users)
    }

    return (
        <div className="container mb-5">
            <div className="card w-50 bg-dark mt-3 mx-auto">
                <div className="row">
                    <div className="col-sm-12 col-md-4" align="center">
                        <FcVoicePresentation size="120" color="teal" className="mx-auto"/>
                    </div>
                    <div className="col-sm-12 col-md-8">
                        <h5 className="card-title text-center text-white">Prueba con API</h5>
                        <p className="card-text text-center text-white">Muestra información a través de rutas al seleccionar un usuario</p>
                    </div>
                </div>
            </div>
            <div className="row justify-center mt-3">
                {
                usuarios.map(item => (
                <div className="col-sm-12 col-md-6 col-xl-4">
                    <div key={item.id} className="card text-white bg-info mt-4">
                        <div className="card-body">
                            <h5 className="text-center"><FcBusinessContact size="40"/>{item.name}</h5>
                            <h6 className="text-center"><FcDepartment size="40"/>{item.company.name}</h6>
                            <p className="text-center mb-n2">{item.company.catchPhrase}</p>
                            <p className="text-center">{item.company.bs}</p>
                            <Link to={`/informacion-usuario/${item.id}`}>
                                <button className="btn-dark btn-block">Ver datos de usuario</button>
                            </Link>
                        </div>
                    </div>
                </div>
                ))
                }
            </div>
        </div>
    )
}
