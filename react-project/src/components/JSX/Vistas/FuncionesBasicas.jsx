import React from 'react';
import Contador from '../../JS/Contador'
import Jsx from '../../JS/Jsx'
import Lista from '../../JS/Lista'
import { FcElectronics } from 'react-icons/fc';

const FuncionesBasicas = () => {

  return (
      <div className="container">
            <div className="card w-50 bg-dark mt-3 mx-auto" width="50rem">
                <div className="row">
                    <div className="col-sm-12 col-md-4" align="center">
                        <FcElectronics size="120" color="teal" className="mx-auto"/>
                    </div>
                    <div className="col-sm-12 col-md-8">
                        <h5 className="card-title text-center text-white">Funciones básicas</h5>
                        <p className="card-text text-center text-white">Contiene algunas estructuras de control</p>
                    </div>
                </div>
            </div>
            <div className="mt-5">
                <div className="row">
                    <div className="col-sm-12 col-lg-4 mt-3">
                        <Jsx/>
                    </div>
                    <div className="col-sm-12 col-lg-4 mt-3">
                        <Contador/>
                    </div>
                    <div className="col-sm-12 col-lg-4 mt-3">
                        <Lista/>
                    </div>
                </div>
            </div>
      </div>
  );
}

export default FuncionesBasicas