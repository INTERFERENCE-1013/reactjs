import React, {useEffect, useState } from 'react'
import { FcBusinessman, FcPhone, FcPortraitMode, FcFeedback } from 'react-icons/fc';
import {Link, useParams} from 'react-router-dom'

const InformacionUsuario = () => {

    const {id} = useParams()

    const [usuario, setUsuario] = useState([])
    // const [usuarioDireccion, setUsuarioDireccion] = useState([])
    // const [usuarioCompania, setUsuarioCompania] = useState([])

    useEffect(() => {
        const obtenerDatos = async () => {
            const data = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
            const user = await data.json()
            setUsuario(user)
            // setUsuarioDireccion(user.company)
            // setUsuarioCompania(user.address)
        }
        obtenerDatos()
    }, [id])

    console.log(usuario)
    return (
        <div className="container mt-5">
            <div className="card w-75 bg-info mx-auto">
                <div className="card-header text-center">
                    Datos de usuario
                </div>
                <div className="card-body">
                    <h5><FcBusinessman size="40"/>Nombre: {usuario.name}</h5>
                    <h5><FcPortraitMode size="40"/>Nombre de usuario: {usuario.username}</h5>
                    <h5><FcPhone size="40"/>Teléfono: {usuario.phone}</h5>
                    <h5><FcFeedback size="40"/>Email: {usuario.email}</h5>
                    {/* <h6 className="text-center"><FcBusinessContact size="40"/>{usuarioCompania.name}</h6> */}
                    <Link to="/api">
                        <button className="btn-dark btn-block">Volver</button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default InformacionUsuario