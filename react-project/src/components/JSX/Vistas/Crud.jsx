import React, {useState} from 'react';
import CrudModule from '../Modulos/CrudModule';
import CreatePerson from '../Modulos/CreatePerson'
import UpdatePerson from '../Modulos/UpdatePerson';
import { v4 as uuidv4 } from 'uuid';
import { FcTodoList } from 'react-icons/fc';

const Crud = () => {

  //Datos del listado por defecto
  const usersData = [
    { id: uuidv4(), nombre: 'Alejandro', apellido: 'Gonzalez' },
    { id: uuidv4(), nombre: 'Alicia', apellido: 'Martinez' },
    { id: uuidv4(), nombre: 'Laura', apellido: 'Ramirez' }
  ]

  //state
  const [users, setUsers] = useState(usersData)

  //Agregar persona
  const createPerson = (user) => {
    user.id = uuidv4()
    setUsers([
      ...users,
      user
    ])
  }

  //Eliminar persona
  const deletePerson = (id) => {
    setUsers(users.filter(user => user.id !== id))
  }

  //Actualizar persona
  const updateUser = (id, updateUser) => {
    setEdition(true)

    setUsers(users.map(user=>(user.id === id ? updateUser : user)))
  }

  //Pre actualizar persona
  const [edition, setEdition] = useState(true)

  const [currentPerson, setCurrentPerson] = useState({
    id: null, nombre: '', apellido: ''
  })

  const updateRow = (user) => {
    setEdition(false)
    setCurrentPerson({
      id: user.id, nombre: user.nombre, apellido: user.apellido
    })
  }

  return (
    <div className="container">
        <div className="card w-50 bg-dark mt-3 mx-auto" width="50rem">
            <div className="row">
                <div className="col-sm-12 col-md-4" align="center">
                    <FcTodoList size="120" color="teal" className="mx-auto"/>
                </div>
                <div className="col-sm-12 col-md-8">
                    <h5 className="card-title text-center text-white">CRUD usando PROPS</h5>
                    <p className="card-text text-center text-white">Permite listar, crear, editar o eliminar usuarios</p>
                </div>
            </div>
        </div>
        <div className="row mt-5" align="center">
            <div className="col-sm-12 col-md-4">
              {
                  edition ? (
                  <div align="center">
                  {/* <h2>NUEVO REGISTRO</h2> */}
                      <CreatePerson createPerson={createPerson}/>
                  </div>
                  ) : (
                  <div align="center">
                      <UpdatePerson
                      currentPerson={currentPerson}
                      updateUser={updateUser}/>
                  </div>
                  )
              }
            </div>
            <div className="col-sm-12 col-md-8">
              {/* Envia datos y funciones a los componentes */}
              <CrudModule
              users={users}
              deletePerson={deletePerson}
              updateRow={updateRow}/>
            </div>
        </div>
    </div>
  );
}

export default Crud