import React from 'react'
import {useForm} from 'react-hook-form'

const CreatePerson = (props) => {

    const {register, errors, handleSubmit} = useForm();

    const onSubmit = (data, e) =>{

        props.createPerson(data)
        //Limpiar campos al enviar formulario
        e.target.reset()
    }

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="card h-100 text-white bg-info mb-3">
                <div className="card-header">NUEVO REGISTRO</div>
                    <div className="card-body">
                    <input
                    name='nombre'
                    placeholder='Nombre'
                    className="form-control my-2"
                    ref={
                        register({
                            required: {value: true, message: 'Nombre obligatorio'},
                            minLenght: {values: 3, message: 'Minimo 3 caracteres' }
                        })
                    }
                    />
                    <span className="text-danger text-small d-block">
                        {errors?.nombre?.message}
                    </span>
                    <input
                    name='apellido'
                    placeholder='Apellido'
                    className="form-control my-2"
                    ref={
                        register({
                            required: {value: true, message: 'Apellido obligatorio'},
                            minLenght: {values: 3, message: 'Minimo 3 caracteres' }
                        })
                    }
                    />
                    <span className="text-danger text-small d-block mb-2">
                        {errors?.apellido?.message}
                    </span>
                    <div align="center">
                        <button className="btn btn-dark mt-4">Agregar</button>
                    </div>
                </div>
            </div>
        </form>
     );
}
export default CreatePerson;