import React from 'react'
import {useForm} from 'react-hook-form'

const UpdatePerson = (props) => {

    const {register, errors, handleSubmit, setValue} = useForm({
        defaultValues: props.currentPerson
    });

    setValue('nombre', props.currentPerson.nombre)
    setValue('apellido', props.currentPerson.apellido)

    const onSubmit = (data, e) =>{

        data.id = props.currentPerson.id

        props.updateUser(props.currentPerson.id, data)
        //Limpiar campos al enviar formulario
        e.target.reset()
    }

    return (

        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="card text-white bg-info mb-3">
                <div className="card-header">EDITAR REGISTRO</div>
                    <div className="card-body">
                    <input
                        name='nombre'
                        placeholder='Nombre'
                        className="form-control my-2"
                        ref={
                            register({
                                required: {value: true, message: 'Nombre obligatorio'},
                                minLenght: {values: 3, message: 'Minimo 3 caracteres' }
                            })
                        }
                    />
                    <span className="text-danger text-small d-block">
                        {errors?.nombre?.message}
                    </span>
                    <input
                        name='apellido'
                        placeholder='Apellido'
                        className="form-control my-2"
                        ref={
                            register({
                                required: {value: true, message: 'Apellido obligatorio'},
                                minLenght: {values: 3, message: 'Minimo 3 caracteres' }
                            })
                        }
                    />
                    <span className="text-danger text-small d-block mb-2">
                        {errors?.apellido?.message}
                    </span>
                    <div align="center">
                        <button className="btn btn-dark mt-4">Editar</button>
                    </div>
                </div>
            </div>
        </form>
     );
}
export default UpdatePerson;