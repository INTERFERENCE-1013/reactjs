import React from 'react'
import { MdClose, MdModeEdit } from 'react-icons/md';

const CrudModule = (props) => {

    return (
        <div className="table-responsive overflow-y-hidden">
            <table className="table table-striped table-dark table-bordered">
                    <thead>
                        <tr>
                            <th className="text-center">Nombre</th>
                            <th className="text-center">Apellido</th>
                            <th className="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            props.users.length > 0 ? (
                                props.users.map( user =>(
                                    <tr key={user.id}>
                                        <td>{user.nombre}</td>
                                        <td>{user.apellido}</td>
                                        <td>
                                            <div>
                                                <div align="center">
                                                    <MdModeEdit
                                                    size="40"
                                                    color="teal"
                                                    onClick={() => props.updateRow(user)}/>
                                                {/* </div>
                                                <div> */}
                                                    <MdClose
                                                    size="40"
                                                    color="red"
                                                    onClick={() => props.deletePerson(user.id)}/>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr colSpan={3}>
                                    <td>Sin registros</td>
                                    <td>Sin registros</td>
                                    <td>Sin registros</td>
                                </tr>
                            )
                        }
                    </tbody>
            </table>
        </div>
    );
}

export default CrudModule;